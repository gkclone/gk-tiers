<?php

/**
 * @file
 *
 * Admin UI for the tier(_type) entities.
 */

/**
 * Entity: tier ================================================================
 */

/**
 * Entity form: tier
 */
function tier_form($form, &$form_state, $tier) {
  $form_state['tier'] = $tier;
  $entity_id = entity_id('tier', $tier);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => $tier->title,
  );

  $form['threshold'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#title' => t('Threshold'),
    '#default_value' => $tier->threshold,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $tier->uid,
  );

  // Field API form elements.
  field_attach_form('tier', $tier, $form, $form_state);

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the tier entity form.
 */
function tier_form_submit($form, &$form_state) {
  $tier = $form_state['tier'];

  entity_form_submit_build_entity('tier', $tier, $form, $form_state);
  entity_save('tier', $tier);

  $uri = entity_uri('tier', $tier);
  $form_state['redirect'] = $uri['path'];

  drupal_set_message(t('Tier %title saved.', array(
    '%title' => entity_label('tier', $tier),
  )));
}

/**
 * Entity: tier_type ===========================================================
 */

/**
 * Entity form: tier_type
 */
function tier_type_form($form, &$form_state, $tier_type, $op = 'edit') {
  if ($op == 'clone') {
    $tier_type->label .= ' (cloned)';
    $tier_type->type = '';
  }

  $form_state['tier_type'] = $tier_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($tier_type->label) ? $tier_type->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($tier_type->type) ? $tier_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $tier_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'gk_tiers_type_list',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($tier_type->description) ? $tier_type->description : '',
    '#description' => t('A description of this type.'),
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the tier_type entity form.
 */
function tier_type_form_submit(&$form, &$form_state) {
  $tier_type = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('tier_type', $tier_type);

  // Redirect the user back to the list of tier_type entities.
  $form_state['redirect'] = 'admin/structure/tier-types';
}
