<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <h2><?php print $title; ?></h2>

    <div class="tier__threshold">
      <span class="threshold__label">Threshold:</span>
      <span class="threshold__value"><?php print $tier->threshold; ?></span>
    </div>

    <div<?php print $content_attributes; ?>>
      <?php print render($content); ?>
    </div>
  </div>
<?php endif; ?>
