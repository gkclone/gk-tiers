<?php

/**
 * @file
 *
 * API functions for the tier(_type) entities.
 */

/**
 * Entity: tier ================================================================
 */

/**
 * Entity access callback: tier
 */
function tier_access($op, $tier, $account = NULL, $entity_type = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  static $administer_tiers = array(
    'create' => 1, 'update' => 1, 'edit' => 1, 'delete' => 1,
  );
  if (isset($administer_tiers[$op])) {
    return user_access('administer tiers', $account);
  }

  if ($op == 'view') {
    return user_access('view published content', $account);
  }
}

/**
 * Entity view callback: tier
 */
function tier_view($tier, $view_mode = 'full', $langcode = NULL, $page = NULL) {
  return entity_view('tier', array($tier), $view_mode, $langcode, $page);
}

/**
 * Entity load callback: tier
 */
function tier_load($tid, $reset = FALSE) {
  $tiers = tier_load_multiple(array($tid), array(), $reset);
  return reset($tiers);
}

/**
 * Entity load multiple callback: tier
 */
function tier_load_multiple($tids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('tier', $tids, $conditions, $reset);
}

/**
 * Entity: tier_type ===========================================================
 */

/**
 * List of tier_type entities.
 */
function gk_tiers_type_list($type_name = NULL) {
  $types = entity_load_multiple_by_name('tier_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Entity access callback (used by admin UI): tier_type
 */
function tier_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer tier types', $account);
}

/**
 * Entity load callback: tier_type
 */
function tier_type_load($tier_type) {
  return gk_tiers_type_list($tier_type);
}
