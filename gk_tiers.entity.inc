<?php

/**
 * @file
 *
 * Entity classes for the tier(_type) entities.
 */

/**
 * Entity class: tier
 */
class Tier extends Entity {
  protected function defaultUri() {
    return array(
      'path' => 'admin/content/tier/' . $this->identifier(),
    );
  }
}
