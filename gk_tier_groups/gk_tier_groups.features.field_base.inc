<?php
/**
 * @file
 * gk_tier_groups.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function gk_tier_groups_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_tier_group_dates'
  $field_bases['field_tier_group_dates'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_tier_group_dates',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'date',
    'settings' => array(
      'cache_count' => 4,
      'cache_enabled' => 0,
      'granularity' => array(
        'day' => 'day',
        'hour' => 0,
        'minute' => 0,
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ),
      'timezone_db' => '',
      'todate' => 'optional',
      'tz_handling' => 'none',
    ),
    'translatable' => 0,
    'type' => 'datetime',
  );

  // Exported field_base: 'field_tier_group_tiers'
  $field_bases['field_tier_group_tiers'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_tier_group_tiers',
    'foreign keys' => array(
      'tier' => array(
        'columns' => array(
          'target_id' => 'tid',
        ),
        'table' => 'tier',
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'direction' => 'ASC',
          'property' => 'threshold',
          'type' => 'property',
        ),
        'target_bundles' => array(),
      ),
      'target_type' => 'tier',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
