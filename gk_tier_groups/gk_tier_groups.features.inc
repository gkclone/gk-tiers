<?php
/**
 * @file
 * gk_tier_groups.features.inc
 */

/**
 * Implements hook_default_tier_group_type().
 */
function gk_tier_groups_default_tier_group_type() {
  $items = array();
  $items['tier_group_type__default'] = entity_import('tier_group_type', '{
    "type" : "tier_group_type__default",
    "label" : "Default",
    "description" : "Default tier group type."
  }');
  return $items;
}
