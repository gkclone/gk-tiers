<?php

/**
 * @file
 * Provide tier groups as content.
 */

$plugin = array(
  'title' => 'Tier group',
);

/**
 * Return info for a particular tier group.
 */
function gk_tier_groups_tier_group_content_type_content_type($subtype) {
  return _gk_tier_groups_tier_group_get_info($subtype);
}

/**
 * Return all tier group content types available.
 */
function gk_tier_groups_tier_group_content_type_content_types() {
  return _gk_tier_groups_tier_group_get_info();
}

/**
 * Render callback.
 */
function gk_tier_groups_tier_group_content_type_render($subtype, $conf) {
  if ($tier_group = tier_group_load($subtype)) {
    return (object) array(
      'content' => tier_group_view($tier_group),
    );
  }
}

/**
 * Admin title callback.
 */
function gk_tier_groups_tier_group_content_type_admin_title($subtype, $conf) {
  if (!$tier_group = _gk_tier_groups_tier_group_get_info($subtype)) {
    return t('Deleted/missing tier group @subtype_id', array(
      '@subtype_id' => $subtype,
    ));
  }

  return t('Tier group: @title', array(
    '@title' => $tier_group['title'],
  ));
}

/**
 * Admin info callback.
 */
function gk_tier_groups_tier_group_content_type_admin_info($subtype, $conf) {
  if ($tier_group = _gk_tier_groups_tier_group_get_info($subtype)) {
    return (object) array(
      'title' => $tier_group['title'],
    );
  }
}

/**
 * Get info for a particular tier group.
 */
function _gk_tier_groups_tier_group_get_info($subtype = NULL) {
  $tier_groups = array();

  $query = db_select('tier_group', 'tg')
    ->fields('tg', array(
      'tgid',
      'type',
      'title',
      'status',
    ))
    ->orderBy('tg.title');

  if (!empty($subtype)) {
    $query->condition('tgid', $subtype);
  }

  $result = $query->execute()->fetchAllAssoc('tgid');

  foreach ($result as $tier_group) {
    $status = $tier_group->status ? '' : ' (unpublished)';

    $tier_groups[$tier_group->tgid] = array(
      'title' => $tier_group->title . $status,
      'icon' => 'icon_' . $tier_group->type . '.png',
      'category' => t('Tier groups'),
    );
  }

  if (array_key_exists($subtype, $tier_groups)) {
    return $tier_groups[$subtype];
  }

  return $tier_groups;
}
