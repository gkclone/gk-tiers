<?php

/**
 * @file
 *
 * Entity classes for the tier_group(_type) entities.
 */

/**
 * Entity class: tier_group
 */
class TierGroup extends Entity {
  protected function defaultUri() {
    return array(
      'path' => 'admin/content/tier/groups/' . $this->identifier(),
    );
  }
}
