<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <h2><?php print $title; ?></h2>

    <div<?php print $content_attributes; ?>>
      <?php print render($content); ?>
    </div>
  </div>
<?php endif; ?>
