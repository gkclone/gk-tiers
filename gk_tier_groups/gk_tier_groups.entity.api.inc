<?php

/**
 * @file
 *
 * API functions for the tier_group(_type) entities.
 */

/**
 * Entity: tier_group ==========================================================
 */

/**
 * Entity access callback: tier_group
 */
function tier_group_access($op, $tier_group, $account = NULL, $entity_type = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  static $administer_tiers = array(
    'create' => 1, 'update' => 1, 'edit' => 1, 'delete' => 1,
  );
  if (isset($administer_tiers[$op])) {
    return user_access('administer tiers', $account);
  }

  if ($op == 'view') {
    return user_access('view published content', $account);
  }
}

/**
 * Entity view callback: tier_group
 */
function tier_group_view($tier_group, $view_mode = 'full', $langcode = NULL, $page = NULL) {
  return entity_view('tier_group', array($tier_group), $view_mode, $langcode, $page);
}

/**
 * Entity load callback: tier_group
 */
function tier_group_load($tgid, $reset = FALSE) {
  $tier_groups = tier_group_load_multiple(array($tgid), array(), $reset);
  return reset($tier_groups);
}

/**
 * Entity load multiple callback: tier_group
 */
function tier_group_load_multiple($tgids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('tier_group', $tgids, $conditions, $reset);
}

/**
 * Entity: tier_group_type =====================================================
 */

/**
 * List of tier_group_type entities.
 */
function gk_tier_groups_type_list($type_name = NULL) {
  $types = entity_load_multiple_by_name('tier_group_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Entity access callback (used by admin UI): tier_group_type
 */
function tier_group_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer tier group types', $account);
}

/**
 * Entity load callback: tier_group_type
 */
function tier_group_type_load($tier_group_type) {
  return gk_tier_groups_type_list($tier_group_type);
}
