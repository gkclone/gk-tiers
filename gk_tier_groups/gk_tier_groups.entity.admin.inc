<?php

/**
 * @file
 *
 * Admin UI for the tier_group(_type) entities.
 */

/**
 * Entity: tier_group ==========================================================
 */

/**
 * Entity form: tier_group
 */
function tier_group_form($form, &$form_state, $tier_group) {
  $form_state['tier_group'] = $tier_group;
  $entity_id = entity_id('tier_group', $tier_group);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => $tier_group->title,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $tier_group->uid,
  );

  // Field API form elements.
  field_attach_form('tier_group', $tier_group, $form, $form_state);

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the tier_group entity form.
 */
function tier_group_form_submit($form, &$form_state) {
  $tier_group = $form_state['tier_group'];

  entity_form_submit_build_entity('tier_group', $tier_group, $form, $form_state);
  entity_save('tier_group', $tier_group);

  $uri = entity_uri('tier_group', $tier_group);
  $form_state['redirect'] = $uri['path'];

  drupal_set_message(t('Tier group %title saved.', array(
    '%title' => entity_label('tier_group', $tier_group),
  )));
}

/**
 * Entity: tier_group_type =====================================================
 */

/**
 * Entity form: tier_group_type
 */
function tier_group_type_form($form, &$form_state, $tier_group_type, $op = 'edit') {
  if ($op == 'clone') {
    $tier_group_type->label .= ' (cloned)';
    $tier_group_type->type = '';
  }

  $form_state['tier_group_type'] = $tier_group_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($tier_group_type->label) ? $tier_group_type->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($tier_group_type->type) ? $tier_group_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $tier_group_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'gk_tier_groups_type_list',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($tier_group_type->description) ? $tier_group_type->description : '',
    '#description' => t('A description of this type.'),
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the tier_group_type entity form.
 */
function tier_group_type_form_submit(&$form, &$form_state) {
  $tier_group_type = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('tier_group_type', $tier_group_type);

  // Redirect the user back to the list of tier_group_type entities.
  $form_state['redirect'] = 'admin/structure/tier-types/tier-group-types';
}
