<?php
/**
 * @file
 * gk_tier_groups.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function gk_tier_groups_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'tier_group-tier_group_type__default-field_tier_group_dates'
  $field_instances['tier_group-tier_group_type__default-field_tier_group_dates'] = array(
    'bundle' => 'tier_group_type__default',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'tier_group',
    'field_name' => 'field_tier_group_dates',
    'label' => 'Dates',
    'required' => 0,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'tier_group-tier_group_type__default-field_tier_group_tiers'
  $field_instances['tier_group-tier_group_type__default-field_tier_group_tiers'] = array(
    'bundle' => 'tier_group_type__default',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'tier_group',
    'field_name' => 'field_tier_group_tiers',
    'label' => 'Tiers',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Dates');
  t('Tiers');

  return $field_instances;
}
