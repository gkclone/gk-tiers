<?php

/**
 * @file
 *
 * Entity controller classes for the tier_group(_type) entities.
 */

/**
 * Entity controller class: tier_group
 */
class TierGroupController extends EntityAPIController {
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'title' => '',
      'threshold' => 0,
      'uid' => $user->uid,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    // Find this groups tiers and attach them to the output.
    if ($tiers_field = field_get_items('tier_group', $entity, 'field_tier_group_tiers')) {
      $tids = array();

      foreach ($tiers_field as $field) {
        $tids[] = $field['target_id'];
      }

      if ($tiers = tier_load_multiple($tids)) {
        foreach ($tiers as $tier) {
          $tier_build = tier_view($tier);
          $content['tiers'][$tier->tid] = reset($tier_build['tier']);
        }
      }
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
