<?php

/**
 * @file
 *
 * Entity controller classes for the tier(_type) entities.
 */

/**
 * Entity controller class: tier
 */
class TierController extends EntityAPIController {
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'title' => '',
      'threshold' => 0,
      'uid' => $user->uid,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return parent::create($values);
  }
}
